#!/usr/bin/env bash
SOURCE="${0}"

PARENT_DIRECTORY="$( dirname $( dirname "${SOURCE}" ))"
DB_DIRECTORY="${PARENT_DIRECTORY}/run/db"
PROJECT=$( basename "${PARENT_DIRECTORY}" )
MONGO_PROJECT_INSTANCE="mongodb-${PROJECT}"

docker pull mongo

docker stop ${MONGO_PROJECT_INSTANCE}
docker rm ${MONGO_PROJECT_INSTANCE}
docker run -d \
-p 27017:27017 -p 28017:28017 \
--name "${MONGO_PROJECT_INSTANCE}" \
-v ${DB_DIRECTORY}:/data/db \
mongo

docker logs -f "${MONGO_PROJECT_INSTANCE}"
