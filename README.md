# Prototype Crud and Analytics

This repository contains a prototype web-application to collect user data
The user data are collected through a form.
The form is generated dynamically through a schema definition
The data are persisted in mongodb.


# Decomposition
 
- Backend
    - expressjs
    - moongose

- Frontend
    - React 
    - Material-UI

## Doc

- https://medium.com/@gethylgeorge/setting-up-a-simple-graphql-server-with-node-express-and-mongoose-ff8a1071af53
- https://neverfriday.com/2018/02/12/graphql-node-js-mongoose-mongodb/
- https://itnext.io/graphql-mongoose-a-design-first-approach-d97b7f0c869
- https://blog.risingstack.com/graffiti-mongoose-mongodb-for-graphql/
- https://www.compose.com/articles/using-graphql-with-mongodb/
- https://exlskills.com/learn-en/courses/aap-graphql-server-with-nodejs-and-mongodb--gql_node_mgo_asap/aap-learn-rPGeaicdiRTn/mongodb-datastore-PTljKgBEnwyR/what-is-mongoose-oHiTenrzJNKH
- https://medium.com/the-ideal-system/graphql-and-mongodb-a-quick-example-34643e637e49

### Mongoose
- [QuickStart](https://mongoosejs.com/docs/index.html)
