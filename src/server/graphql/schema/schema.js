const graphql = require('graphql');
const _ = require('lodash');
const {
  GraphQLObjectType,
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLString,
  GraphQLID,
  GraphQLList,
  GraphQLEnumType,
  GraphQLSchema
} = graphql;
const {ScoreEnum, EvaluationTypeProperties, EvaluationType} = require('../type/type.js');

const NickNames = [
  'Gabriella',
  'Giorgia',
  'Valerie',
  'Shere',
  'Annalisa',
  'Claudia Col',
  'Claudia Die',
  'Mihaela',
  'Mariana',
  'Gemma',
  'Luana',
  'Chiara Gel',
  'Sonja',
  'Adriana',
  'Ruxandra',
  'Stefania',
  'Madli',
  'Wei yee',
  'Katherine',
  'Purti',
  'Emily',
  'Diana',
  'Laura',
  'Yvonne Mok',
  'Maya Nakamoto',
  'Chiara Pas',
  'Rosella',
  'Sarah Robins',
  'Nina',
  'Silvia Segale',
  'Nives',
  'Jana',
  'Margherita',
  'Monica',
];


function createRandom(i) {
  const result = {id: i.toString(),nickname: NickNames[i]};

  for (let property of EvaluationTypeProperties) {
    const index = Math.floor( Math.random() * ScoreEnum.getValues().length);
    result[property] = ScoreEnum.getValues()[index].name;
  }
  return result;
}
const records = new Array(5);

for (let i = 0; i < 5; i++) {
  const record = createRandom(i)
  console.log(record);
  records.push(record);
}
const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    records: {
      type: EvaluationType,
      args: {id: {type: GraphQLString}},
      resolve(parentValue, args) {
       // return _.find(records,{id: args.id});
          return
      },
    },
  },
});
module.exports = new GraphQLSchema({query: RootQuery});
