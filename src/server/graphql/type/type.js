const {GraphQLString, GraphQLEnumType, GraphQLObjectType} = require('graphql');

const ScoreEnum = new GraphQLEnumType({
  name: 'Score',
  values: {

    TOP: {value: 'TOP'},
    GREAT: {value: 'GREAT'},
    GOOD: {value: 'GOOD'},
    OK: {value: 'OK'},

    NOT_OK: {value: 'NOT_OK'},
    BAD: {value: 'BAD'},
    REALLY_BAD: {value: 'REALLY_BAD'},
    WORST: {value: 'WORST'},

    DO_NOT_KNOW: {value: 'DO_NOT_KNOW'},

  },
});



const EvaluationType = new GraphQLObjectType({
  name: 'EvaluationType',
  fields: {

    id: {type: GraphQLString},
    nickname: {type: GraphQLString},

    boobs: {type: ScoreEnum},
    legs: {type: ScoreEnum},
    hairStyle: {type: ScoreEnum},
    lips: {type: ScoreEnum},
    eyes: {type: ScoreEnum},
    face: {type: ScoreEnum},
    shape: {type: ScoreEnum},
    skin: {type: ScoreEnum},
    style: {type: ScoreEnum},
    voice: {type: ScoreEnum},
    smile: {type: ScoreEnum},
    social: {type: ScoreEnum},
    charm: {type: ScoreEnum},

  },
});
module.exports = {EvaluationType, ScoreEnum};
