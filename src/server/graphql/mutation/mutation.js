const {EvaluationType, EvaluationTypeProperties, ScoreEnum} = require('../type/type.js');
const {GraphQLObjectType} = require('graphql');
const {service} = require('../service.js');

const EvaluationMutationArgs = {};

for( let property of EvaluationTypeProperties) {
  EvaluationMutationArgs[property] = {type : ScoreEnum}
}

const EvaluationMutation = new GraphQLObjectType ( {
  type: EvaluationType,
  name: "EvaluationMutationMutation",
  args : {
    EvaluationMutationArgs
  },
  resolve(parentValue, args, request) {
    const obj = new EvalutionType();
    Object.assign(obj, args);
    console.log(obj);
    service.save(obj);
  }
});

module.exports = EvalutionMutation;
