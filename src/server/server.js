const express = require('express');
const expressGraphQL = require('express-graphql');
const schema = require('./schema/schema.js');
const mutation = require('./mutation/mutation.js');
// const path = require('path');
// const bodyParser = require('body-parser');
// const cors = require('cors');
// const service = require('service');
//
// class Model {
//
// }

function main(ip, port) {
  const app = express();
  const graphQLConf = {
    schema: schema,
    mutation: mutation,
    graphiql: true,
  };
  app.use('/graphql', expressGraphQL(graphQLConf));
  app.use('/', (request, response, next) => response.redirect(301, '/graphql'));

  app.listen(port, (err) => {
        if (err) {
          console.error('Unable to start the server on ip {ip} and port {port}');
        } else {
          console.info('Server starting up on ip {ip} and port {port}');
        }
      },
  );
}

const port = process.env.PF_PORT ? process.env.PF_PORT : 3000;
const ip = process.env.PF_IP ? process.env.PF_IP : '0.0.0.0';
main(ip, port);
