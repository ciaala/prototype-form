const mongoose = require('mongoose');

class EntitiesService {
  static DEFAULT_PORT = 27017;
  static DEFAULT_HOSTNAME = 'localhost';
  static DEFAULT_DATABASE = 'test';

  /**
   *
   * @param {string} hostname
   * @param {number} port
   * @param {string} database
   */
  constructor(hostname, port, database) {
    const _hostname = hostname ? hostname : EntitiesService.DEFAULT_HOSTNAME;
    const _port = port ? port : EntitiesService.DEFAULT_PORT;
    const _database = database ? database : EntitiesService.DEFAULT_DATABASE;
    const connectionString = `mongodb://${_hostname}:${_port}/${_database}`;
    console.debug('[persistence] Connecting to MongoDB: ', connectionString);
    this.connection = mongoose.connect(connectionString, {useNewUrlParser: true});
    this.connection.then(() => {
      this.isConnected = true;
      console.info('[persistence] Connected !');
    }).catch(() => {
      this.isConnected = false;
      console.error('[persistence] Not Connected');
    });
  }

  /**
   *
   * @param model
   * @param schema
   * @returns {Model}
   */
  buildType(model, schema) {
    if (this.isConnected) {
      const _schema = mongoose.Schema(schema);
      return mongoose.model(model, _schema);
    }
  }
  find() {
    
  }
}
module.exports = EntitiesService;
