const mongoose = require('mongoose');
const {EvaluationTypeProperties} = require('../shared/type.js');
const schema = { };
for ( let property of EvaluationTypeProperties) {
  schema[property] = String;
}

const EvaluationSchema = new mongoose.Schema(schema);
module.exports = {EvaluationSchema};
